# tk-battleship-07022020


## Getting Started
```
yarn install
yarn start
```

## Description
Battle ship app game.

## Initial Data
```
const HAS_VISIBLE_SHIPS = true;

const SHIP_TYPES = {
  CARRIER: {
    name: "Carrier",
    shipType: "carrier",
    size: 5
  },
  BATTLECHIP: {
    name: "Battleship",
    shipType: "battleship",
    size: 4
  },
  DESTROYER: {
    name: "Destroyer",
    shipType: "destroyer",
    size: 3
  },
  SUBMARINE: {
    name: "Submarine",
    shipType: "submarine",
    size: 2
  },
  PATROL_BOAT: {
    name: "Patrol Boat",
    shipType: "patrolBoat",
    size: 1
  }
};

const LEVELS = {
  EASY: {
    name: "Easy",
    maxTurns: 100, // because is the number of total blocks :)
  },
  MEDIUM: {
    name: "Medium",
    maxTurns: 100,
  },
  HARD: {
    name: "Hard",
    maxTurns: 50,
  }
}
```

## Folder structure
```
src  
│
└───app
│   │
│   └───components
│   │   │
|   |   └─── basic
|   |   |   
|   |   └─── complex
│   │
│   └───screens
│       │
|       └─── Game
|            | 
|            | index.tsx 
|            | 
|            | constants.ts
|            | 
|            | utils.ts
|            | 
|            | utils.test.ts
|            | 
|            | style.module.scss
|            | 
|            └───components
|            |    |
|            |    └───Board
|            |    | 
|            |    └───Levels
|            |    | 
|            |    └───Notice
|            |
|            └─── img
│
└───assets
|
└───constants
│
└───services
│
└───types
│
└───utils
│
└───scss
    │
    └───base
    │
    └───settings

```
___