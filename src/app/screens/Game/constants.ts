export const HAS_VISIBLE_SHIPS = true;

export const DIRECTION = {
  VERTICAL: "VERTICAL",
  HORIZONTAL: "HORIZONTAL"
};

export const ROW_KEYS = ['A','B','C','D', 'E', 'F', 'G', 'H', 'I', 'J'];

export const SHIP_TYPES = {
  CARRIER: {
    name: "Carrier",
    shipType: "carrier",
    size: 5
  },
  BATTLECHIP: {
    name: "Battleship",
    shipType: "battleship",
    size: 4
  },
  DESTROYER: {
    name: "Destroyer",
    shipType: "destroyer",
    size: 3
  },
  SUBMARINE: {
    name: "Submarine",
    shipType: "submarine",
    size: 2
  },
  PATROL_BOAT: {
    name: "Patrol Boat",
    shipType: "patrolBoat",
    size: 1
  }
};

export const SHIPS = {
  BATTLECHIP_1: SHIP_TYPES.BATTLECHIP,
  DESTROYER_1: SHIP_TYPES.DESTROYER,
  DESTROYER_2: SHIP_TYPES.DESTROYER,
  SUBMARINE_1: SHIP_TYPES.SUBMARINE,
  SUBMARINE_2: SHIP_TYPES.SUBMARINE,
  SUBMARINE_3: SHIP_TYPES.SUBMARINE,
  PATROL_BOAT_1: SHIP_TYPES.PATROL_BOAT,
  PATROL_BOAT_2: SHIP_TYPES.PATROL_BOAT,
  PATROL_BOAT_3: SHIP_TYPES.PATROL_BOAT,
  PATROL_BOAT_4: SHIP_TYPES.PATROL_BOAT,
}

export const LEVELS = {
  EASY: {
    name: "Easy",
    maxTurns: 100, // because is the number of total blocks :)
  },
  MEDIUM: {
    name: "Medium",
    maxTurns: 100,
  },
  HARD: {
    name: "Hard",
    maxTurns: 50,
  }
}