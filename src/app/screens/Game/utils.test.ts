import {
  getInitialBoard,
  getInitialGameData,
  isAvailablePosition,
  addShip,
  getUpdatedGameData
} from "./utils";

import { SHIPS, DIRECTION } from "./constants";

const testBlock = {
  hasShip: true,
  isAttacked: true,
  shipType: SHIPS.DESTROYER_1.shipType
};

const COORDINATE_TEST_1 = { x: 0, y: 0 };
const COORDINATE_TEST_2 = { x: 3, y: 0 };
const COORDINATE_TEST_3 = { x: 6, y: 0 };
const COORDINATE_TEST_4 = { x: 8, y: 0 };

const updatedGameData = () => {
  return { ...getInitialGameData(), turns: 1, shipsAttacked: 1 };
};

const boardWithHorizontalShip = () => {
  const testBoard = getInitialBoard();
  const value = {
    hasShip: true,
    isAttacked: false,
    shipType: SHIPS.DESTROYER_1.shipType
  };
  testBoard[0][0] = value;
  testBoard[0][1] = value;
  testBoard[0][2] = value;
  return testBoard;
};

const boardWithVerticalShip = () => {
  const testBoard = getInitialBoard();
  const value = {
    hasShip: true,
    isAttacked: false,
    shipType: SHIPS.DESTROYER_1.shipType
  };
  testBoard[0][0] = value;
  testBoard[1][0] = value;
  testBoard[2][0] = value;
  return testBoard;
};

test("Ship is in range", () => {
  expect(
    isAvailablePosition(
      COORDINATE_TEST_3,
      SHIPS.DESTROYER_1,
      DIRECTION.HORIZONTAL,
      getInitialBoard()
    )
  ).toBe(true);
});

test("Ship is not in range", () => {
  expect(
    isAvailablePosition(
      COORDINATE_TEST_4,
      SHIPS.DESTROYER_1,
      DIRECTION.HORIZONTAL,
      getInitialBoard()
    )
  ).toBe(false);
});

test("Ship block is occupied", () => {
  expect(
    isAvailablePosition(
      COORDINATE_TEST_1,
      SHIPS.DESTROYER_1,
      DIRECTION.HORIZONTAL,
      boardWithHorizontalShip()
    )
  ).toBe(false);
});

test("Ship block is not occupied", () => {
  expect(
    isAvailablePosition(
      COORDINATE_TEST_2,
      SHIPS.DESTROYER_1,
      DIRECTION.HORIZONTAL,
      boardWithHorizontalShip()
    )
  ).toBe(true);
});

test("Add horizontal ship", () => {
  expect(
    addShip(
      COORDINATE_TEST_1,
      SHIPS.DESTROYER_1,
      DIRECTION.HORIZONTAL,
      getInitialBoard()
    )
  ).toStrictEqual(boardWithHorizontalShip());
});

test("Add vertical ship", () => {
  expect(
    addShip(
      COORDINATE_TEST_1,
      SHIPS.DESTROYER_1,
      DIRECTION.VERTICAL,
      getInitialBoard()
    )
  ).toStrictEqual(boardWithVerticalShip());
});

test("Update game data", () => {
  expect(getUpdatedGameData(getInitialGameData(), testBlock)).toStrictEqual(
    updatedGameData()
  );
});
