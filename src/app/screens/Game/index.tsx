import React, { FC, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import i18next from "i18next";
import gameActions from "redux/game/actions";
import Nav from "app/components/complex/Nav";
import Board from "./components/Board";
import Levels from "./components/Levels";
import Notice from "./components/Notice";
import { HAS_VISIBLE_SHIPS } from "./constants";
import {
  getInitialBoard,
  getInitialGameData,
  getInmutableBoard,
  getInmutableRanking,
  getShipsArr,
  getBoardWithRamdomShips,
  getBoardWithAttackedShip,
  getUpdatedGameData,
  getUpdatedRanking
} from "./utils";

import styles from "./styles.module.scss";

const Game: FC = () => {
  const [boardState, setBoardState] = useState(getInitialBoard());
  const [gameDataState, setGameDataState] = useState(getInitialGameData());
  const [shipVisibilityState, setShipVisibilityState] = useState(
    HAS_VISIBLE_SHIPS
  );

  const rankingData = useSelector((state: any) => state.gameReducer.ranking);
  const dispatch = useDispatch();

  const resetGame = () => {
    setBoardState(getInitialBoard());
    setGameDataState(getInitialGameData());
  };

  const updateRanking = (newScore: {}) => {
    const ranking: any = getInmutableRanking(rankingData);
    const updatedRanking = getUpdatedRanking(newScore, ranking);

    localStorage.setItem("ranking", JSON.stringify(updatedRanking));
    dispatch(gameActions.addRanking(updatedRanking));
  };

  const updateGameData = (gameData: any, updatedBlock: any) => {
    const updatedGameData = getUpdatedGameData(gameData, updatedBlock);

    if (updatedGameData.isFinishedGame) {
      setBoardState(getInitialBoard());
      setGameDataState({ ...updatedGameData, maxTurns: 0 });
      updateRanking(updatedGameData);
    } else {
      setGameDataState(updatedGameData);
    }
  };

  const handleAttack = (e: any) => {
    e.preventDefault();

    const board = getInmutableBoard(boardState);
    const coordinate = JSON.parse(e.target.dataset.coordinate);
    const updatedBoard = getBoardWithAttackedShip(coordinate, board);
    setBoardState(updatedBoard);

    const gameData = { ...gameDataState };
    const updatedBlock = updatedBoard[coordinate.y][coordinate.x];
    updateGameData(gameData, updatedBlock);
  };

  const addShipsRandomly = () => {
    if (!gameDataState.maxTurns) return;

    const board = getInmutableBoard(boardState);
    const shipsArr = getShipsArr();
    const updatedBoard = getBoardWithRamdomShips(shipsArr, board);
    setBoardState(updatedBoard);
  };

  const setRanking = () => {
    const rankingCache: string | null = localStorage.getItem("ranking");

    if (!rankingData.length && rankingCache) {
      dispatch(gameActions.addRanking(JSON.parse(rankingCache)));
    }
  };

  useEffect(addShipsRandomly, [gameDataState.maxTurns]);
  useEffect(setRanking, []);

  return (
    <div className={styles.container}>
      <Nav />
      <h1 className="title has-text-centered">{i18next.t("app:name")}</h1>
      <Notice gameDataState={gameDataState} />
      {gameDataState.maxTurns ? (
        <Board
          boardState={boardState}
          handleAttack={handleAttack}
          addShipsRandomly={addShipsRandomly}
          shipVisibilityState={shipVisibilityState}
          setShipVisibilityState={setShipVisibilityState}
          resetGame={resetGame}
        />
      ) : (
        <Levels setGameDataState={setGameDataState} />
      )}
    </div>
  );
};

export default Game;
