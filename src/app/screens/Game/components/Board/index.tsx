import React, { FC } from "react";
import i18next from "i18next";
import { ROW_KEYS } from "../../constants";
import { columnsKeysArr } from "../../utils";
import { IBoard } from "types/app";

import styles from "./styles.module.scss";

const Board: FC<IBoard> = ({
  boardState,
  handleAttack,
  shipVisibilityState,
  setShipVisibilityState,
  resetGame,
}) => {
  const handleShipsVisibility = () => {
    setShipVisibilityState(!shipVisibilityState);
  };

  const renderColumnKey = (_: any, columnIndex: number) => {
    return (
      <span className={styles.columnKey} key={`columnsKeys_${columnIndex}`}>
        {columnIndex + 1}
      </span>
    );
  };
  const renderRow = (row: any, rowIndex: number) => {
    const renderBlock = (block: any, blockIndex: number) => (
      <div key={`block_${rowIndex}_${blockIndex}`}>
        <button
          type="button"
          className={`${styles.block} ${
            shipVisibilityState ? styles[block.shipType] : ""
          } ${block.hasShip && block.isAttacked ? styles.sunk : ""}`}
          data-coordinate={JSON.stringify({ x: blockIndex, y: rowIndex })}
          onClick={handleAttack}
          disabled={block.isAttacked}
        ></button>
      </div>
    );

    return (
      <div className={styles.row} key={`row_${rowIndex}`}>
        <span className={styles.rowKey}>{ROW_KEYS[rowIndex]}</span>
        {row.map(renderBlock)}
      </div>
    );
  };

  return (
    <div className={styles.container}>
      <div className={styles.columnsKeys}>
        {columnsKeysArr.map(renderColumnKey)}
      </div>
      <div className={styles.board}>{boardState.map(renderRow)}</div>
      <div className="columns">
        <div className="column is-offset-2 is-4">
          <button className="button is-primary is-fullwidth" onClick={handleShipsVisibility}>
            {i18next.t("game:showHideShips")}
          </button>
        </div>
        <div className="column is-offset-1 is-4">
          <button className="button is-primary is-fullwidth" onClick={resetGame}>
            {i18next.t("game:resetGame")}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Board;
