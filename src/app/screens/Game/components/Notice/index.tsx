import React, { FC } from "react";

import styles from "./styles.module.scss";

interface INotice {
  gameDataState: {
    turns: number;
    shipsAttacked: number;
    isFinishedGame: boolean;
    isWonGame: boolean;
  };
}

const Notice: FC<INotice> = ({ gameDataState }) => {
  return (
    <div className={styles.container}>
      {gameDataState.isWonGame && (
        <div className="notification is-success is-light">You win!</div>
      )}
      {gameDataState.isFinishedGame && !gameDataState.isWonGame && (
        <div className="notification is-danger is-light">You haven't won yet</div>
      )}
      {gameDataState.isFinishedGame && (
        <div className="notification is-info is-light">
          Finished Game, try again!
        </div>
      )}
    </div>
  );
};

export default Notice;
