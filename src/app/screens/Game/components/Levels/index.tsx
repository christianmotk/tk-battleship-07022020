import React, { FC, useState } from "react";
import i18next from "i18next";
import Input from "app/components/basic/Input";
import { getLevelsArr, getInitialGameData } from "app/screens/Game/utils";
import { updateFieldState, setInitialState } from "utils/form";
import { ILevels } from "types/app";
import { IEventChange } from "types/common";
import { LEVELS } from "constants/fieldNames";

import styles from "./styles.module.scss";

const Levels: FC<ILevels> = ({ setGameDataState }) => {
  const [levelDataState, setLevelDataState] = useState(setInitialState(LEVELS));

  const handleMaxTurn = (e: any) => {
    e.preventDefault();
    const userName = levelDataState[LEVELS.USER_NAME].value;
    const initialGameData = {
      ...getInitialGameData(),
      ...(userName && { userName }),
      maxTurns: +e.target.value
    };
    setGameDataState(initialGameData);
  };

  const handleChange = (e: IEventChange) =>
    updateFieldState(e, levelDataState, setLevelDataState);

  const renderLevel = (
    level: { name: string; maxTurns: number },
    levelIndex: number
  ) => {
    return (
      <button
        type="button"
        className="button is-primary is-fullwidth mb10"
        onClick={handleMaxTurn}
        value={level.maxTurns}
        key={`level_${levelIndex}`}
      >
        {level.name}
      </button>
    );
  };

  return (
    <div className={styles.container}>
      <div className="mb20">
        <Input
          name={LEVELS.USER_NAME}
          type="text"
          placeholder="Username (optional)"
          onChange={handleChange}
          value={levelDataState[LEVELS.USER_NAME].value}
          customInputClassName="input"
          customContainerClassName="field"
          errorMessage={levelDataState[LEVELS.USER_NAME].errorMessage}
        />
      </div>
      <h2 className="subtitle has-text-centered">{i18next.t("game:chooseLevelOption")}</h2>
      <div className="mb20">{getLevelsArr.map(renderLevel)}</div>
      <h2 className="subtitle has-text-centered">{i18next.t("game:enterCustomMaxTurns")}</h2>
      <form className="mb20">
        <Input
          name={LEVELS.MAX_TURNS}
          type="number"
          max={100}
          min={20}
          placeholder="Max turns"
          onChange={handleChange}
          value={levelDataState[LEVELS.MAX_TURNS].value}
          customInputClassName="input"
          customContainerClassName="field"
          errorMessage={levelDataState[LEVELS.MAX_TURNS].errorMessage}
        />
        <button
          type="submit"
          className="button is-primary is-fullwidth mb10"
          onClick={handleMaxTurn}
          value={levelDataState[LEVELS.MAX_TURNS].value}
        >
          Start
        </button>
      </form>
    </div>
  );
};

export default Levels;
