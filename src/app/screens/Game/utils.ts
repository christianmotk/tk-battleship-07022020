import { SHIPS, SHIP_TYPES, LEVELS, DIRECTION } from "./constants";
import { TBoard, TShip, TGameData, TBlock, TCoordinate } from "types/app";

export const getInitialBoard: any = () => {
  const board = [];
  for (let i = 0; i < 10; i++) {
    const initialValues = {
      hasShip: false,
      isAttacked: false,
      shipType: ""
    };
    board.push(new Array(10).fill(initialValues));
  }

  return board;
};

export const getInitialGameData = () => ({
  userName: "Anonymous",
  turns: 0,
  maxTurns: 0,
  shipsAttacked: 0,
  isFinishedGame: false,
  isWonGame: false
});

export const columnsKeysArr = new Array(10)
  .fill(0)
  .map((_, index) => index + 1);

export const getShipTypesArr = () => Object.values(SHIP_TYPES);

export const getShipsArr = () => Object.values(SHIPS);

export const getLevelsArr = Object.values(LEVELS);

export const getTotalShipsLength = () =>
  getShipsArr().reduce((acc, ship) => acc + ship.size, 0);

export const getInmutableBoard = (value: any) =>
  value.map((row: []) => row.map(block => block));

export const getInmutableRanking = (value: [{}]) =>
  value.map(item => ({ ...item }));

export const getRandomCoordinate = () => {
  const max = 99;
  const min = 0;
  const coordinateArr = `0${Math.floor(Math.random() * (max - min) + min)}`
    .substr(-2)
    .toString()
    .split("");
  const coordinate = { x: +coordinateArr[0], y: +coordinateArr[1] };

  return coordinate;
};

export const getRandomDirection = () => {
  const direction =
    Math.floor(Math.random() * 2) === 0
      ? DIRECTION.HORIZONTAL
      : DIRECTION.VERTICAL;

  return direction;
};

export const getRandomShip = () => {
  const shipTypeArrIndex = Math.floor(Math.random() * 5);
  const shipTypesArr = getShipTypesArr();

  return shipTypesArr[shipTypeArrIndex];
};

export const getPositionData = (
  coordinate: { x: number; y: number },
  ship: {},
  direction: string,
  i: number
) => {
  const isHorizontal = direction === DIRECTION.HORIZONTAL;

  return {
    maxPosition: isHorizontal ? coordinate.x : coordinate.y,
    coordinateY: isHorizontal ? coordinate.y : coordinate.y + i,
    coordinateX: isHorizontal ? coordinate.x + i : coordinate.x
  };
};

export const isAvailablePosition = (
  coordinate: any,
  ship: TShip,
  direction: string,
  board: TBoard
) => {
  let isAvailable = true;

  for (let i = 0; i < ship.size; i++) {
    const positionData = getPositionData(coordinate, ship, direction, i);
    const isOutOfRange = positionData.maxPosition > 9 - ship.size;
    const hasShip =
      board[positionData.coordinateY][positionData.coordinateX].hasShip;

    if (isOutOfRange || hasShip) {
      isAvailable = false;
      break;
    }
  }

  return isAvailable;
};

export const addShip = (
  coordinate: TCoordinate,
  ship: TShip,
  direction: string,
  board: TBoard
) => {
  for (let i = 0; i < ship.size; i++) {
    const positionData = getPositionData(coordinate, ship, direction, i);
    const updatedBlock =
      board[positionData.coordinateY][positionData.coordinateX];

    board[positionData.coordinateY][positionData.coordinateX] = {
      ...updatedBlock,
      hasShip: true,
      shipType: ship.shipType
    };
  }

  return board;
};

export const addRandomShip = (ship: TShip, board: TBoard) => {
  const direction = getRandomDirection();
  const coordinate = getRandomCoordinate();

  if (isAvailablePosition(coordinate, ship, direction, board)) {
    board = addShip(coordinate, ship, direction, board);
  } else {
    addRandomShip(ship, board);
  }
};

export const getBoardWithRamdomShips = (shipsArr: any, board: TBoard) => {
  shipsArr.forEach((ship: TShip) => {
    addRandomShip(ship, board);
  });

  return board;
};

export const getUpdatedRanking = (newScore: any, ranking: any) => {
  ranking.push(newScore);
  ranking
    .sort((a: { turns: number }, b: { turns: number }) => a.turns - b.turns)
    .sort(
      (a: { shipsAttacked: number }, b: { shipsAttacked: number }) =>
        b.shipsAttacked - a.shipsAttacked
    );
  return ranking;
};

export const getBoardWithAttackedShip = (coordinate: TCoordinate, board: TBoard) => {
  const updatedBlock = board[coordinate.y][coordinate.x];

  board[coordinate.y][coordinate.x] = {
    ...updatedBlock,
    isAttacked: true
  };

  return board;
};

export const getUpdatedGameData = (gameData: TGameData, updatedBlock: TBlock) => {
  const turns = gameData.turns + 1;
  const shipsAttacked = updatedBlock.hasShip
    ? gameData.shipsAttacked + 1
    : gameData.shipsAttacked;
  const isWonGame = shipsAttacked === getTotalShipsLength();
  const isFinishedGame = turns === gameData.maxTurns || isWonGame;

  const updatedGameData = {
    ...gameData,
    turns,
    shipsAttacked,
    isWonGame,
    isFinishedGame
  };

  return updatedGameData;
};
