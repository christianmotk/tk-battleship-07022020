import React, { FC, useEffect } from "react";
import i18next from "i18next";
import { useSelector, useDispatch } from "react-redux";
import gameActions from "redux/game/actions";
import Nav from "app/components/complex/Nav";

import styles from "./styles.module.scss";

const Ranking: FC = () => {
  const rankingData = useSelector((state: any) => state.gameReducer.ranking);
  const dispatch = useDispatch();

  const setRanking = () => {
    const rankingCache: any = localStorage.getItem("ranking");
    if (!rankingData.length && rankingCache) {
      dispatch(gameActions.addRanking(JSON.parse(rankingCache)));
    }
  };

  const renderScore = (score: any, scoreIndex: number) => {
    return (
      <tr key={`score_${scoreIndex}`}>
        <td>{scoreIndex + 1}</td>
        <td>{score.userName}</td>
        <td>{score.shipsAttacked}</td>
        <td>{score.turns}</td>
        <td>{score.isWonGame ? "Winner" : "Not winner"}</td>
      </tr>
    );
  };

  useEffect(setRanking, []);

  return (
    <div className={styles.container}>
      <Nav />
      <h1 className="title has-text-centered">Ranking</h1>
      <table className="table is-striped is-fullwidth is-bordered">
        <thead>
          <tr>
            <th>{i18next.t("ranking:position")}</th>
            <th>{i18next.t("ranking:username")}</th>
            <th>{i18next.t("ranking:shipsAttacked")}</th>
            <th>{i18next.t("ranking:turns")}</th>
            <th>{i18next.t("ranking:status")}</th>
          </tr>
        </thead>
        <tbody>{rankingData.map(renderScore)}</tbody>
      </table>
    </div>
  );
};

export default Ranking;
