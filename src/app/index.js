import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import store from "redux/store";
import Game from "app/screens/Game";
import Ranking from "app/screens/Ranking";
import ROUTES from "constants/routes";
import 'config/i18n';

import "normalize.css";
import "bulma";
import "scss/base/index.scss";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route path={ROUTES.ROOT} component={Game} exact />
          <Route path={ROUTES.GAME} component={Game} />
          <Route path={ROUTES.RANKING} component={Ranking} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
