import React from 'react';
import { Link } from 'react-router-dom';
import i18next from 'i18next';
import ROUTES from 'constants/routes';

import styles from './styles.module.scss';

function Nav() {
  return (
    <nav className={styles.container}>
      <ul className={styles.menu}>
        <li><Link to={ROUTES.GAME}>{i18next.t("app:game")}</Link></li>
        <li><Link to={ROUTES.RANKING}>{i18next.t("app:ranking")}</Link></li>
      </ul>
    </nav>
  );
}

export default Nav;
