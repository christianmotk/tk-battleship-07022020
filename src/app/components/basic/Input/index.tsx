import React, { FC } from "react";

import styles from "./styles.module.scss";

interface IInput {
  name: string;
  value: string;
  placeholder: string;
  type?: string;
  min?: number;
  max?: number;
  maxLength?: number;
  inputRef?: any;
  onChange: any;
  customContainerClassName: string;
  customInputClassName: string;
  errorMessage: string;
  rules?: string;
  disabled?: boolean;
}

const Input: FC<IInput> = ({
  name,
  type,
  value,
  placeholder,
  min,
  max,
  maxLength,
  inputRef,
  onChange,
  disabled,
  customInputClassName,
  customContainerClassName,
  errorMessage,
  rules
}) => {
  return (
    <div className={`${styles.container} ${customContainerClassName}`}>
      <input
        name={name}
        value={value}
        type={type}
        placeholder={placeholder}
        min={min}
        max={max}
        maxLength={maxLength}
        data-rules={rules}
        ref={inputRef}
        onChange={onChange}
        className={`${styles.input} ${customInputClassName} ${errorMessage &&
          "is-danger"}`}
        disabled={disabled}
      />
      {errorMessage && <div className="help is-danger">{errorMessage}</div>}
    </div>
  );
};

export default Input;
