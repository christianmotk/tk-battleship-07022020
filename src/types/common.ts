export interface IEventChange {
  target: HTMLInputElement;
}
export interface IfieldState {
  [key: string]: {
    value: string;
    errorMessage: string;
  };
}

