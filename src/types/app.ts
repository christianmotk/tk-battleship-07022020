export type TShip = {
  name: string;
  shipType: string;
  size: number;
};

export type TBoard = [
  [
    {
      hasShip: boolean;
      isAttacked: boolean;
      shipType: string;
    }
  ]
];

export type TGameData = {
  userName: string;
  turns: number;
  maxTurns: number;
  shipsAttacked: number;
  isFinishedGame: boolean;
  isWonGame: boolean;
};

export type TBlock = {
  hasShip: boolean;
  isAttacked: boolean;
  shipType: string;
};

export type TCoordinate = {
  x: number;
  y: number;
};

export interface ILevels {
  setGameDataState: any;
}

export interface IBoard {
  boardState: TBoard;
  handleAttack: any;
  addShipsRandomly: () => void;
  shipVisibilityState: boolean;
  setShipVisibilityState: any;
  resetGame: any,
}

