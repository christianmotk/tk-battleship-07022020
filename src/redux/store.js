import {
  combineReducers, createStore, applyMiddleware, compose,
} from 'redux';

import gameReducer from 'redux/game/reducer';
import responseMiddleware from 'redux-response-middleware';

const reducers = combineReducers({ gameReducer });

const middlewares = [];
const enhancers = [];

middlewares.push(responseMiddleware());

enhancers.push(applyMiddleware(...middlewares));

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line

export default createStore(reducers, composeEnhancers(...enhancers));
