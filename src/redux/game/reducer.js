import { ADD_RANKING } from "constants/actionNames";

const initialState = {
  ranking: []
};

const gameReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_RANKING:
      return { ...state, ranking: payload };
    default:
      return state;
  }
};

export default gameReducer;
