import { ADD_RANKING } from "constants/actionNames";

const gameActions = {
  addRanking: payload => ({
    type: ADD_RANKING,
    payload
  })
};

export default gameActions;
