export const POST_FIELDS = {
  USER_NAME: 'userName',
};

export const LEVELS = {
  USER_NAME: 'username',
  MAX_TURNS: 'maxTurns',
}