const ROUTES = {
  ROOT: '/',
  GAME: '/game',
  RANKING: '/ranking',
};

export default ROUTES;
