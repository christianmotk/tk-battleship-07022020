import i18next from "i18next";

i18next.init({
  lng: "en",
  resources: {
    en: {
      app: {
        name: "BattleShip",
        game: "Game",
        ranking: "Ranking"
      },
      game: {
        chooseLevelOption: "Choose a level option:",
        showHideShips: "Show/Hide Ships",
        resetGame: "Reset game",
        enterCustomMaxTurns: "Or enter custom max turns value:"
      },
      ranking: {
        position: "Position",
        username: "Username",
        shipsAttacked: "Ships attacked",
        turns: "Turns",
        status: "Status"
      }
    }
  },
  initImmediate: false
});
