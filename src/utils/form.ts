import { IEventChange, IfieldState } from "types/common";

export const setInitialState = (fields:any) => Object.keys(fields).reduce((acc: any, key) => {
  acc[fields[key]] = {
    value: '',
    errorMessage: '',
  };

  return acc;
}, {});

export const updateFieldState = (e: IEventChange, state: IfieldState, updateState: any) => {
  const field = e.target;
  const updatedState = { ...state[field.name], value: field.value };
  updateState({ ...state, [field.name]: updatedState });
};
